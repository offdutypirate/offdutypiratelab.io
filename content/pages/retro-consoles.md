---
title: Retro Consoles and Computers
date: 2025-01-27
modified: 2025-01-27
status: draft
---

Just a list of retro consoles and computers I own.  I plan to add more details to this over time, but for the moment just wanted to collect it somewhere. 

## Consoles

* NES
* SNES
* Sega Genesis
* Sega CD
* Atari 2600
* Colecovision

## Computers

* Commodore 64
* Tandy TRS-80 Model 100
* Tandy TRS-80 ColorComputer 1
* Tandy TRS-80 ColorComputer 2
* PowerMac G4
* PowerBook G4
