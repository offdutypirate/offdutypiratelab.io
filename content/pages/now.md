---
title: Now
date: 2024-06-04
modified: 2025-01-15
status: published
---

A very brief overview of my activities and focus in the last few months. You can also read more about the [now page concept](https://nownownow.com/about).

## What am I doing for fun?

- Collecting vintage computer hardware
- Buying more LEGO than I can build

## What am I doing for work?

- Working with Kerberos and LDAP.  SSO, back before it was hip.
- Trying to avoid AI as much as possible.