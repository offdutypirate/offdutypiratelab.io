---
title: Reading List
date: 2021-07-03
modified: 2025-01-15
status: published
---

Sample of books that I've read and enjoyed enough to add here.  It's not exhaustive or complete and will likely not be updated any an reasonable amount of time.

- [Tiger Chair](https://www.goodreads.com/book/show/209608728-tiger-chair) by Max Brooks.
- [Hell Divers](https://nicholassansburysmith.com/book/hell-divers/)
- [Hell Divers II: Ghosts](https://nicholassansburysmith.com/book/ghosts/)
