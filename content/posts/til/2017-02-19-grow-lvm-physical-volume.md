---
Title: Grow LVM physical volume
Date: 2017-02-19
Status: published
---

The pvresize command can be used to grow an existing physical volume so long as
you grow the parition on the underlying device.

1. Use `fdisk -l /dev/sdc1` to find the start sector of the partition.
2. Delete existing partition and create a new partition with the same start
sector.
3. Use `pvresize /dev/sdc1` to grow current physical volume.