---
Title: Modify iptables rules
Date: 2017-10-03
---

Show all iptables rules and include line numbers

```
iptables -L INPUT --line-numbers
```

Replace iptables rule at a specific line number

```
iptables -R X INPUT <new rule here>
```
