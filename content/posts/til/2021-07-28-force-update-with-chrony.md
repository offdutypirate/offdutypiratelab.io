---
title: Force chrony to update time
date: 2021-07-28 10:00:00
---

``` {.sourceCode .shell}
$ chronyc -a 'burst 4/10'
$ sleep 5
$ chronyc -a makestep
```

References
----------

[chrony - chronyc(1)](https://chrony.tuxfamily.org/doc/4.1/chronyc.html#burst)
