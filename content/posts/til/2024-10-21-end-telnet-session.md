---
title: End an active telnet session
date: 2024-10-21
slug: end-an-active-telnet-session
summary: Use CTRL+] and q to end an active telnet session
status: published
category: memo
---

End an active telnet session with `CTRL+]` and then `quit`.

```text
Trying 192.168.0.222...
Connected to 192.168.0.222.
Escape character is '^]'.

User Access Verification

telnet> quit
Connection closed.
```

Commands may be abbreviated, so `quit` and `q` both work.
