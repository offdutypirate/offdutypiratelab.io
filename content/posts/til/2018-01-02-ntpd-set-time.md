---
title: Set time with NTPd
date: 2018-01-01
---

Use `ntpd -gc` to start the NTP daemon and set the time regardless of what
the offset between the local and remote clock is.
