---
title: Using yum with only the cache
date: 2021-09-08
---

Yum keeps a local cache.  To use yum with only the cache use `yum -C` or `yum --cacheonly`. 

## Reference
man yum
