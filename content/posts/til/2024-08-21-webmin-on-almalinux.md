---
title: Installing Webmin on AlmaLinux
date: 2024-08-21
keywords: webmin, almalinux
status: published
summary: Notes on install Webmin on AlmaLinux
category: til
---

Installing Webmin on AlmaLinux is fairly straightforward.  Just follow the [installation documentation,](https://webmin.com/download/) and you're mostly done. 

I do disable the local root account by default, so I can't immediately log in to Webmin.  Webmin does allow any user that can sudo ALL to login, but this is disabled by default.  To enable it, we also need to install some extra packages. 

    dnf config-manager --set-enabled crb
    dnf install perl-IO-Tty

Then in `/etc/webmin/miniserv.conf` enable sudo

    $ cat /etc/webmin/miniserv.conf
    ...
    sudo=1
    ...

After this, restart Webmin, and you can log in using a user with sudo access.
