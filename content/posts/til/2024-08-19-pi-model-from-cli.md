---
title: Determine Raspberry Pi model from command line
tags: raspberrypi
date: 2024-08-19
keywords: raspberrypi
status: published
summary: Determine Raspberry Pi model from command line
category: til
---

Quick way to determine what model of Raspberry Pi you have from the command line.

    cat /sys/firmware/devicetree/base/model 
    Raspberry Pi 4 Model B Rev 1.5sysop@pi5:~$ 


