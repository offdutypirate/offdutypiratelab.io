---
category: til
tags: systemd
date: 2024-07-10 14:20:00
description: 
keywords: systemd
status: published
summary: With systemd, it is easy to configure a service to automatically restart when it's crashed.
title:  Auto-restart a crashed systemd service
slug: auto-restart-a-crashed-systemd-service
related-posts: using-healthchecksio-with-systemd-timers
---

With systemd, it is easy to configure a service to automatically restart when
it's crashed.

Here's a typical unit file.  It's fairly basic, but you get the idea. With this
unit, if the service dies or otherwise fails, systemd will leave it alone.

    [Unit]
    Description=Example Daemon
    After=network-online.target
    Wants=network-online.target systemd-networkd-wait-online.service
    
    [Service]
    ExecStart=/usr/local/bin/example-service

    [Install]
    WantedBy=multi-user.target


Adding the `Restart` option to the `[Service]` stanza will tell systemd to
instead auto-restart the service when it fails or is killed. 

    [Unit]
    Description=Example Daemon
    After=network-online.target
    Wants=network-online.target systemd-networkd-wait-online.service
    
    [Service]
    Restart=on-failure
    RestartSec=5s

    ExecStart=/usr/local/bin/example-service

    [Install]
    WantedBy=multi-user.target
