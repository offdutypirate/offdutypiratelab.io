---
title: Generate Public SSH Key From Private SSH Key
description: Get a public SSH key from a Private SSH key
date: 2024-01-12 14:30:00
tags: ssh
related_posts: enable-ssh-port-forwarding-after-opening-session
---

If you have a private SSH key but need to regenerate the public key again, use
`ssh-keygen` with `-y`. This option will read a private OpenSSH format file and
print an OpenSSH public key to stdout.  

```
ssh-keygen -y -f ~/.ssh/id_ed25519 > ~/.ssh/id_ed25519.pub
Enter passphrase: 
```

This will save the public key to ~/.ssh/id_ed25519.pub.
