---
title: Install Asterisk on Debian 12
date: 2024-10-21
tags: asterisk
keywords: asterisk
slug: install-asterisk-on-debian-12
summary: Notes on installing Asterisk on Debian 12
status: draft
---

For reason[1], if you want Asterisk on Debian you will need to install from source.

A project I've recently been working on has me needing to setup Asterisk again.
What's here is just a few of the notes I've had along the way that deviate from the
official install from source guide[2] that covers most of this already.

[1] https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1031046
[2] https://docs.asterisk.org/Getting-Started/Installing-Asterisk/Installing-Asterisk-From-Source/Prerequisites/Checking-Asterisk-Requirements/
