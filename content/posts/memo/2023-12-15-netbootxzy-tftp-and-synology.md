---
title: PXE netboot.xyz with Unifi UDM-Pro and Synology TFTP 
description: Setup PXE network boot using netboot.xyx, Synology TFPT server, and Unifi UDM-Pro DHCP 
date: 2023-12-15 07:45:00
tags: synology, unifi, tftp, netboot
category: memo
---

> [netboot.xyz](https://netboot.xyz) lets you PXE boot various operating system installers or utilities from a single tool over the network.

— <small><a href="https://netboot.xyz/">netboot.xyz</a></small>

## Enable TFPT service on Synology DSM

* Control Panel > File Services > Advanced
* Check "Enable TFTP service"
* Select TFPT root folder
* Apply

You can edit advanced settings to enable logging, configure permissions, and limit clients. 

## Enable Network Boot while using Unifi UDM-Pro DHCP Server

* Unifi Network > Settings > Networks
* Select approrate network, likely "Default"
* DHCP Service Management
* Network Boot: Server IP is the Synology NAS and Filename should be the netboot.xyz file. 

## Copy the `netboot.xyz` file to the Synology. 

See the [netboot.xyz documentation](https://netboot.xyz/docs/booting/tftp) and download the the [netboot.xyz boot file](https://boot.netboot.xyz/ipxe/netboot.xyz.kpxe)[^1]. 

## Upload this file to the TFPT root folder.

Upload the netboot.xyz files to the above configured TFPT root folder.  

For more information on netboot.zyz refer to their documentation [here](https://netboot.xyz/docs/).  

[^1]: This link _should_ point to the latest file, but may not.  Also go to the source to be sure.

