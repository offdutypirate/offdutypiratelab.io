---
title: Terminal Server using Cisco NM-32A Async Network Modules
category: memo
tags: cisco
date: 2024-08-15
keywords: cisco
status: draft
summary: Using a Cisco NM-32A Async Network Modules to build a terminal access server.
---

Using a Cisco NM-32A Async Network Modules installed in a Cisco router you can build a simple terminal access server.

