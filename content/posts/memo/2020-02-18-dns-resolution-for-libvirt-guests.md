---
title: Automatic DNS resolution for libvirt guests using a local domain
date: 2020-02-19 20:50:00
---

Configuring libvirt
-------------------

First libvirt needs to be configured. Modify the default network using
virsh net-edit default.

```
$ sudo virsh net-dumpxml default
<network connections='1'>
  <name>default</name>
  <uuid>3a3627c2-15a7-4e7f-b7b6-0c2310ba9427</uuid>
  <forward mode='nat'>
    <nat>
      <port start='1024' end='65535'/>
    </nat>
  </forward>
  <bridge name='virbr0' stp='on' delay='0'/>
  <mac address='52:54:00:fa:cb:e5'/>
  <domain name='lab.offdutypirate.com' localOnly='yes'/>
  <ip address='192.168.122.1' netmask='255.255.255.0'>
    <dhcp>
      <range start='192.168.122.128' end='192.168.122.254'/>
    </dhcp>
  </ip>
</network>
```

Configuring the guests
----------------------

```
$ hostnamectl set-hostname bigbox.lab.offdutypirate.com
```

Configuring NetworkManager
--------------------------

The NetworkManager configuration must be updated to enable the internal
dnsmasq service as well as configuring the domain set above. Modify the
configuration files as below with the correct domain name and subnet.

```
# add to /etc/NetworkManager.conf

[main]
dns=dnsmasq}
# add to /etc/NetworkManager/dnsmasq.d/libvirt_dnsmasq.conf
server=/lab.offdutypirate.com/192.168.122.1
```

References
----------

- <https://fabianlee.org/2018/10/22/kvm-using-dnsmasq-for-libvirt-dns-resolution/>
- <https://serverfault.com/questions/855476/kvm-libvirt-how-to-make-local-dhcp-leases-available-to-local-dns-server-through>
- <https://wiki.libvirt.org/page/VirtualNetworking>
- <https://liquidat.wordpress.com/2017/03/03/howto-automated-dns-resolution-for-kvmlibvirt-guests-with-a-local-domain/>
