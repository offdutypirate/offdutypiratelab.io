---
title: Octoprint Plugins
date: 2022-04-20 08:00:00
---

Just a list of the 3rd party plugins I have installed in Octoprint.  Keeping these here for my own future reference. 

* ABL Expert Plugin
* Bed Visualizer
* DiscordRemote
* Filament Manager
* OctoDash Companion
* Octolapse
* PrettyGCode
* Print History Plugin
* PrintTimeGenius
* Slicer Thumbnails
* UI Customizer