---
title: Reset and Clone libvirt/KVM guest virtual machines
date: 2020-02-20 19:50:00
---

Prepare the clone
-----------------

Use the virt-sysprep command to reset a virtual machine. The virtual
machine must be shut down before you use this command.

``` {.sourceCode .shell}
virt-sysprep --connect qemu:///system -d centos7-gold
```

Create the clone
----------------

Use the virt-clone command to create a clone of an existing virtual
machine.

``` {.sourceCode .shell}
virt-clone  --original centos7-gold --name centos7-01 --auto-clone
```

References
----------

-   <http://libguestfs.org/virt-sysprep.1.html>
-   <http://libguestfs.org/>
