---
Title: Why should you install upgrades?
Date: 2020-11-15 17:00:00
status: draft
---

There are a few reasons to apply updates released by the vendors.

Resolve a known security issue
------------------------------

Often updates will remediate known security issues. Updates that include
security related fixes should always be install. How quickly these are
installed should be a function of the criticality of the issue and the
likihood it impacts your environment.

Resolve a know bug
------------------

All software has bugs. If the software you are running is impacted by a
bug that has been patched the update should most likely be installed.

Make a new feature avaiable
---------------------------
