---
title: Swap Floppy Disks or CD-ROMs using QEMU monitor
date: 2024-09-12
tags: qemu, virt
keywords: 
category:
slug: swap-floppy-disks-or-cd-rom-images
summary: Use QEMU monitor to swap floppy or CD images
status: published
---

Use the QEMU monitor by entering `CTRL+ALT+2`. 

Run `info block` to see the device name.  Most likely floppy0 in newer versions.  However, older versions and older guides suggest `fda`.

Use the `change` command to switch the image

```
(qemu) change floppy "DISK1.IMG"
==> [silently accepted]
```

Go back with `CTRL+ALT+1`.

For a CD the process is almost identical.  Instead of using `floppy0` or `fda`, the device is likely `ide1-cd0`, `sd0`, or similar.:w
