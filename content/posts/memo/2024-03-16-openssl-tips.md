---
category: memo
date: 2024-03-16 12:30:00
description: Small collection of some useful openssl commands when working with certificates
tags: crypto, tls, certs
title: Useful OpenSSL Tips
modified: 2024-04-16
---

[TOC]

Just writing down some common openssl command I often find myself using.  One
day this collection might be more complete and better organized.  For now, it's
just this. 

Some common options when viewing a certificate with `openssl x509`

## Examine a single certificate

The most basic command for viewing a certificate.  

    openssl x509 -text -in files/cert.crt -noout

We use `-text` to print the contents of the certificate and `-noout` to not
print the encoded certificate.


## Examine a file that contains multiple certificates

```
while \
  openssl x509 -noout -serial -dates -subject -nameopt multiline ; \
do : ; \
done < /path/to/file-with-multiple-certs.crt
```

`-serial`
: prints the certificates serial

`-dates`
: prints the valid to and from dates

`-subject`
: prints the certificate subject

`-nameopt multiline`
: causes the names to printed on mulitple lines instead of a single long line

## Verify a certificate is valid using a specific root.

    openssl verify \
    -trusted files/certs/root-ca.pem \
    -untrusted files/certs/intermediate-1.pem \
    -untrusted files/certs/intermediate-1.pem \
    -show_chain \
    files/server.crt

!!! note
    the `-untrusted` here is to include the certificate in the chain, but does
    not trust it.  This would be the certificates that your server is configured
    to serve up in its chain.  So, all authorities that have signed it up to but
    **not** including the root certificate.

## Check that the certificate, and chain, being served is valid

```
openssl s_client \
-CAfile files/certs/root-ca.pem \
-servername www.example.com \
-connect www.example.com:443 2>&1 < /dev/null
```

!!! note
    Here `-CAfile` is to set a specific root certificate to use.  If the
    expected root is in your local truststore, you can omit this.

