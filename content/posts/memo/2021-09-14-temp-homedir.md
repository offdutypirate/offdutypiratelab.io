---
Title: Making a temporary home directory
Date: 2021-09-14
---

There are times when your home directory might not be available.  This can be
due to your homedir coming from LDAP or another network service and the local
directory not being available.  When logging in you'll see something similar
to

```
Last login: Mon Sep 13 21:03:37 from foo.example.com Could no chdir to home
directory /home/example/user
[user@bar /]$
```


There are several ways to work around this.  Creating `/home/example/user` is
one, but you might not want this to be permanent.

```
export HOME=`mktemp -d` 
cd $HOME
```

Will create a temporary directory and make that $HOME.  This doesn't resolve
all issues with a missing home directory, but it's a start.

## Reference
man mktemp

