---
Title: Configuring ipmi using ipmitool
Date: 2016-08-10
Status: published
---

## Packages and Services


On RHEL7Server the `OpenIPMI` and `ipmitool` are necessary to configure and use IPMI.

```
$ yum install OpenIMPI ipmitool
```
    
## Networking

Configure a static ip address on the first port.  For dynamic configuration
set `ipsrc` to dynamic.

```
$ sudo ipmitool lan set 1 ipsrc static
$ sudo ipmitool lan set 1 ipaddr 203.0.113.45
$ sudo ipmitool lan set 1 netmask 255.255.255.0
$ sudo ipmitool lan set 1 defgw ipsrc 203.0.113.3
```


## User Configuration

Add a new user with admin rights

```
$ sudo ipmitool user set name 2 admin
$ sudo ipmitool user set password 2
Password for user 2: 
Password for user 2: 
$ sudo ipmitool channel setaccess 1 2 link=on ipmi=on callin=on privilege=4
$ sudo ipmitool user enable 2
```


## Other Topics
* Configuration authenication
* View and clear SEL
* View hardware status