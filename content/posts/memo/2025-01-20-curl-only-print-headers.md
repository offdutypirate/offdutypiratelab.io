---
title: Printing only headers with cURL
date: 2025-01-20
tags:
keywords:
slug: curl-only-print-headers
summary: Using cURL only print headers and not entire page content
status: published
---

Working with cURL you can get lots of useful and detailed information.  Sometimes though, it's a bit too much.  

For my case I only want to see the headers.

```sh
$ curl -v https://isthereaninjainyourhouse.com
```

However, this also includes the content as well, which I don't want.  So let's remove that.

```sh
$ curl -v -s https://isthereaninjainyourhouse.com/ 1> /dev/null

  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0* Host isthereaninjainyourhouse.com:443 was resolved.
* IPv6: (none)
* IPv4: 167.99.239.150
*   Trying 167.99.239.150:443...
* Connected to isthereaninjainyourhouse.com (167.99.239.150) port 443

 ...

} [5 bytes data]
> GET / HTTP/2
> Host: isthereaninjainyourhouse.com
> User-Agent: curl/8.9.1
> Accept: */*
> 
* Request completely sent off

...

* Connection #0 to host isthereaninjainyourhouse.com left intact

```

Here, we redirect stdout to /dev/null and leave only stderr, which is where headers are sent. 

However, there is a simplier way

```sh
curl --head https://isthereaninjainyourhouse.com/

HTTP/2 200 
last-modified: Sat, 26 Aug 2023 14:24:59 GMT
etag: "74e-603d439dd04c0"
accept-ranges: bytes
content-length: 1870
content-type: text/html; charset=UTF-8
date: Tue, 21 Jan 2025 18:00:04 GMT
server: Apache
```

We can one step more and get both headers and server/client messages, but still skip on content. 

```sh
curl -v --head https://isthereaninjainyourhouse.com/
* Host isthereaninjainyourhouse.com:443 was resolved.
* IPv6: (none)
* IPv4: 167.99.239.150
 ...
* [HTTP/2] [1] OPENED stream for https://isthereaninjainyourhouse.com/
* [HTTP/2] [1] [:method: HEAD]
* [HTTP/2] [1] [:scheme: https]
* [HTTP/2] [1] [:authority: isthereaninjainyourhouse.com]
* [HTTP/2] [1] [:path: /]
* [HTTP/2] [1] [user-agent: curl/8.9.1]
* [HTTP/2] [1] [accept: */*]
> HEAD / HTTP/2
> Host: isthereaninjainyourhouse.com
> User-Agent: curl/8.9.1
> Accept: */*
> 
* Request completely sent off
* TLSv1.3 (IN), TLS handshake, Newsession Ticket (4):
* TLSv1.3 (IN), TLS handshake, Newsession Ticket (4):
< HTTP/2 200 
HTTP/2 200 
< last-modified: Sat, 26 Aug 2023 14:24:59 GMT
last-modified: Sat, 26 Aug 2023 14:24:59 GMT

...

< content-type: text/html; charset=UTF-8
content-type: text/html; charset=UTF-8
< date: Tue, 21 Jan 2025 18:00:28 GMT
date: Tue, 21 Jan 2025 18:00:28 GMT
< server: Apache
server: Apache
< 

* Connection #0 to host isthereaninjainyourhouse.com left intact
```
