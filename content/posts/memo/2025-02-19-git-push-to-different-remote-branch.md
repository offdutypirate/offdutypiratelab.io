---
title: How to push to a different remote branch with Git
date: 2025-02-19
tags: git
keywords: 
slug: git-push-to-different-remote-branch
summary: 
status: published
category: til
---

I often find myself commit changes to main that I should of put in a different branch.  This is especially difficult when using any of the git forges that have main protected.  

```sh
git push origin main:feature-branch
```

The above is fairly self-explanatory.  After we've commited our changes to the local main branch, we'll push those to the remote feature-branch. 

From here you can open a merge request to merge feature-branch into the remote main.
