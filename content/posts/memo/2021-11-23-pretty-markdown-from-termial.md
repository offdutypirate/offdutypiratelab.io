---
title: View Markdown from Terminal
date: 2021-11-23 10:00:00
---

You can view markdown files using any file viewer.  However, to get a slightly more pleasent view you can use pandoc to first convert to HTML and then render that with either lynx or w3m (any any other browser.) 

Examples:

``` {.sourceCode .shell}
# Using pandoc with Lynx
$ pandoc file.md | lynx -stdin

# Using pandoc with w3m
$ pandoc file.md | w3m -T text/html
```

This can easily be aliased with your shell or used with a function with little effort.
