---
title: Renew Expired GPG Keys
date: 2018-01-22
---

To renew an expiring, or extend the expiration of a gpg key edit the key
and set a new expiration date.

```
gpg --edit-key <keyid>
command> list
command> key [keyid]
command> expire
command> save
```

Where `keyid` above is the key that is expiring.
