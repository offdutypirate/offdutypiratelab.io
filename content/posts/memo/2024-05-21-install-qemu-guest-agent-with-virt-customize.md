---
title: Installing qemu-guest-agent with virt-customize
description: Using virt-customize to install qemu-guest-agent when using cloud images
date: 2024-05-21 10:00:00
tags: proxmox, virt
---

When using cloud-images from various distrobutions, sometimes the qemu-guest-agent isn't installed.  Using virt-customize you config modify the image to include this package as well as enable it boot. 

```
$ virt-customize -a /path/to/image --install qemu-guest-agent --run-command '/usr/bin/systemctl enable qemu-guest-agent'
```

The guest should not be running when you do this.  
