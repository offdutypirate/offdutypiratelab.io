---
title: Reinstall the Kernel with yum
date: 2016-07-20
status: Published
related_posts: rpm-notes
---

After failing to update the initrd after making changes to the plymouth boot I was unable to boot into the kernel.  The yum reinstall fixed this

    $ yum reinstall kernel
    
However, this doesn't always work.  If yum feels the kernel is already installed and healthy this will return 'nothing to do' from yum.  In that instance, it is necessary to remove the kernel first.

    $ yum remove kernel-<version>

And then, reinstall

    $ yum reinstall kernel-<version>

Also see [this post]({filename}/posts/memo/2015-11-22-a-few-rpm-notes.md)