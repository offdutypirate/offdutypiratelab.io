---
title: Cisco: Disconnect from service-module session
category: memo
tags: cisco
date: 2024-08-12
keywords: cisco
status: published
summary: When connected to a service module you need to press Ctrl+Shift+6, then X in order to go back to the host router.
---

When you have entered a service-module session, for example connecting to NM-16ES-1G-P switch module, and you would like to disconnect and return back to the host router you need to press `Ctrl+Shift+6`, then `X`.

You will see any output when you press `Ctrl+Shift+6`, but when you press “X”, you’ll drop back to the host router session.

The session to the service module will still be active in the background. Hitting Enter will return you to the service module.  To disconnect type `service-module gigabitEthernet X/X session clear` where `X/X` is the port of the service module.  Or you can simply type `disconnect`.