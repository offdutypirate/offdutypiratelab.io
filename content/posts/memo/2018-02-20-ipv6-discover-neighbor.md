---
Title:  Discover IPv6 Neighbors
Date: 2018-02-20
---


Ping nodes on the local network segment using multicast[^1]

``` {.sourceCode .none}
ping6 -c2 -I <interface> ff02::1
```

Then, view IPv6 neighbors

``` {.sourceCode .none}
ip -f inet6 neigh
```

References
----------

[^1]: <https://en.wikipedia.org/wiki/Multicast_address>
