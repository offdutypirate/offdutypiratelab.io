---
title: Using Healthchecks.io with systemd timers
category: til
date: 2022-05-01 09:00:00
slug: using-healthchecksio-with-systemd-timers
related_posts: auto-restart-a-crashed-systemd-service
---

I use [healthchecks.io](https://healthchecks.io/about/) to monitor several cronjobs and this works great.

Recently I have been using systemd timers instead of cronjobs but wanted to continue to use 
healthchecks with them.  I accomplished this using `ExecStartPre` and `ExecStartPost` in the service file.

To add these checks to the `dnf-automatic.service`, 

1. Edit the service file `systemctl edit dnf-automatic.service`
2. Add a `[Service]` section
3. Add a `ExecStartPre=` and `ExecStartPost` directive calling healthchecks.io


``` {.sourceCode .shell}
[root@mx1 dnf]# cat /etc/systemd/system/dnf-automatic.service.d/override.conf
[Service]
ExecStartPre=/usr/bin/curl -fsS -m 10 --retry 5 -o /dev/null https://hc-ping.com/${UUID}/start
ExecStopPost=/usr/bin/curl -fsS -m 10 --retry 5 -o /dev/null https://hc-ping.com/${UUID}
```

By adding `/start` to the curl command, you can [log the run time](https://healthchecks.io/docs/measuring_script_run_time/) of the service.  

I am sure there is a better way of doing this, but this is working well for me today.  
