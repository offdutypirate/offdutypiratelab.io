---
title: Enable SSH Port Forwarding after opening session
date: 2024-08-23
keywords: ssh, commandline
status: published
summary: Enabling Port Forwarding during an active SSH session. 
category: memo
related_posts: generate-public-ssh-key-from-private-ssh-key, kill-a-hung-ssh-session
tags: ssh
slug: ssh-portforwarding-after-opening-session
---

When `EscapeChar` has not been disabled, and `EnableEscapeCommandline` is enabled it's possible to use the SSH command line to enabled Port Forwarding in an already active SSH session. 

Usually `EscapeChar` is enabled by default and set to `~`, so we should only need to add `EnableEscapeCommandline` to our local ssh_config. 

    $ cat ~/.ssh/config
    Host
      EnableEscapeCommandline yes

Once this has been done, in a new SSH connection typing `~` and then `C` to open the SSH command line.  From here, we can add a new Port Forwarding using the same syntax as what's used in the SSH command.

    [sysop@wolf ~]$ ~C
    ssh> -L10080:localhost:80
    Forwarding port.

I am showing ~C, but this is not echoed to the screen and just here for the example.

You can view the currently active forwarding with the `#` escape sequence.  

    $ ~#
    The following connections are open:
    #0 client-session (t4 [session] r0 i0/0 o0/0 e[write]/4 fd 4/5/6 sock -1 cc -1 io 0x01/0x01)
    #3 direct-tcpip: listening port 10080 for localhost port 80, connect from ::1 port 41394 to ::1 port 10080 (t4 [direct-tcpip] r1 i0/0 o0/2097152 e[closed]/0 fd 9/9/-1 sock 9 cc -1 io 0x03/0x00)
