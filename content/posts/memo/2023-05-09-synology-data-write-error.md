---
title: Synology Storage Analyzer A data write error
description: Storage Analyzer A data write error occurred while generating the usage report. 
date: 2023-05-09 07:45:00
tags: synology
---

After many months of using Synology Storage Analyzer to create automatic reports recently these have been failing with the error

```
Storage Analyzer: A data write error occurred while generating the usage report. 
```

A bit of searching online and this seems to be common for a few reasons.  In my case, this was caused by having a LDAP server configured that was no longer online. 

To remove the configuration go to Control Panel > Domain/LDAP and either fix or remove any invalid Domain/LDAP servers.
