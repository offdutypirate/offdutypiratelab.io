---
Title: Using screen as a serial console
date: 2018-02-20 11:59:00
---

Using screen you can connect to any serial device in /dev. A typical
USB-to-RS-232 adapter will usually be /dev/ttyUSB0. A physical serial
port will usually be /dev/ttyS0.

``` 
screen /dev/ttyUSB0 9600
```
