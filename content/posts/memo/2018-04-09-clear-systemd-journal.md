---
Title: Clear systemd journal
date: 2018-04-09 12:03:00
lastmod: 2022-09-30 15:00:00
---

If the journal is configured to archive entires these can take up
unnecessary disk space when not properly maintained. The current disk
usage can be seen with journalctl `--disk-usage`. This includes both
active and archived journal files.

## Clear journal entries older then X number of days


```
$ journalctl --vacuum-time=5d
```

## Clear journals exceeding X storage

```
$ journalctl --vacuum-size=2G
```

## Flush journal to disk


Vacuuming only impacts journals that have been written to disk. It may
be necessary to first flush in-memory journal entires to disk before a
vacuum reduces disk usage.

```
$ journalctl --rotate --flush
```
