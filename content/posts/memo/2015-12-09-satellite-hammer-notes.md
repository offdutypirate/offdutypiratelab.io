---
Title: Hammer notes
Date: 2015-12-09
Status: published
---

Here is a collection of my notes on using hammer cli with Red Hat 
Satellite.  Most of these things probably work with Katello also, but 
all of my testing is with Satellite.

## Add new Product

Products are a group of repositories.  Content Hosts can subscribe to a 
product to make those repositories available.

```   
$ hammer product create --name="Product Name" \
  --organization="Default Organization" \
  --description="Description about Product" 
```

## Content Views

```
$ hammer content-view list --organization="Default Organization"
```