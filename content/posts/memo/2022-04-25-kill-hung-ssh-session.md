---
title: Kill a hung SSH session
date: 2022-04-25 08:00:00
---

End a hung ssh sessions with `~` + `.`.  

In practice this doesn't also ways work for me.  I've found that sometimes you
must press Enter a couple of times before `~.` seems to do anything.  
