---
title: Retrieve Existing Keytabs from FreeIPA
description: Creating keytabs 
date: 2022-08-30 09:00:00
status: published
tags: freeipa, kerberos
slug: retreive-existing-keytabs-from-ipa
---

> *This is a work in progress.*

# Introduction

In this example, `s1.prod.example.com` is a real server and an IPA client. The
name `ipa.example.com` is an alias. 

The same process can be repeated to allow additional real hosts to retrieve a
keytab for `ipa.example.com`, simply replace `s1.prod.example.com` with a
different hostname.

    ;; ANSWER SECTION:
    ipa.example.com 30   IN      CNAME   s1.prod.example.com.
    s1.prod.example.com. 124 IN A 203.0.113.88


## Allow host access to retrieve service keytabs

We need to allow the host(s) access to retrieve keytabs for each service.

This can be done from any IPA client or the Web UI.  Normally, this will not be
ran from the host where keytabs will be retrieved, as this operation requires
admin access to IPA.  These commands will need to be repeated each host needing
to retrieve the keytab(s).

    $ ipa service-allow-retrieve-keytab HTTP/ipa.example.com \ 
      --hosts=s1.prod.example.com
    $ ipa service-allow-retrieve-keytab HTTP/s1.prod.example.com \ 
      --hosts=s1.prod.example.com

In the previous step, we allowed the host to retrieve the keytabs for both
service principals.

    [root@s1 ~]# kinit -kt /etc/krb5.keytab

Here's the current keytab used by httpd.  Notice that only keys for
HTTP/$(hostname) are present.

    [root@s1 ~]# klist  -kt /etc/httpd/httpd.keytab
    Keytab name: FILE:/etc/httpd/httpd.keytab
    KVNO Timestamp           Principal
    ---- ------------------- ------------------------------------------------------
       1 04/06/2022 20:40:24 HTTP/s1.prod.example.com@IPA.EXAMPLE.COM
       1 04/06/2022 20:40:24 HTTP/s1.prod.example.com@IPA.EXAMPLE.COM

Use `ipa-getkeytab -r` to retrieve the existing keys and save to `-k
/etc/httpd/httpd.keytab`.

    [root@s1 ~]# ipa-getkeytab -r -p HTTP/ipa.example.com@IPA.EXAMPLE.COM -k
    /etc/httpd/httpd.keytab Keytab successfully retrieved and stored in:
    /etc/httpd/httpd.keytab 

And now, the existing keytab had the new keys added to it for the vanity name.

    [root@s1 ~]# klist  -kt /etc/httpd/httpd.keytab
    Keytab name: FILE:/etc/httpd/httpd.keytab
    KVNO Timestamp           Principal
    ---- ------------------- ------------------------------------------------------
       1 04/06/2022 20:40:24 HTTP/s1.prod.example.com@IPA.EXAMPLE.COM
       1 04/06/2022 20:40:24 HTTP/s1.prod.example.com@IPA.EXAMPLE.COM
       1 08/30/2022 18:59:14 HTTP/ipa.example.com@IPA.EXAMPLE.COM
       1 08/30/2022 18:59:14 HTTP/ipa.example.com@IPA.EXAMPLE.COM

## Allow host access to retrieve host principals

To create keytabs for host principals the process is also very similar.

We will instead used `host-allow-retrieve-keytab` to allow the hosts access to
retrieve the keytab(s).

    ipa host-allow-retrieve-keytab ipa.example.com --hosts=s1.prod.example.com
