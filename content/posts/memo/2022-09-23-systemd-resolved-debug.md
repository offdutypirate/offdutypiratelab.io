---
title: Debug logging with systemd-resolved
description: Enabled debug logs for systemd-resolved 
date: 2022-09-23 18:01:00
status: published
tags: systemd, logging
---

1. Add an override. `sudo systemctl edit systemd-resolved`
```
[Service]
Environment=SYSTEMD_LOG_LEVEL=debug
```
2. Make changes active. `sudo systemctl restart systemd-resolved`
3. View logs. `journalctl -u systemd-resolved -f`


And when you're done, undo all these changes.
```
sudo systemctl revert systemd-resolved
sudo systemctl restart systemd-resolved`
```
