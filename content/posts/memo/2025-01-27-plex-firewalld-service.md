---
title: Create a firewalld service for Plex Media Server
date: 2025-01-27
tags: plex, firewalld
keywords: 
slug: firewalld-service-for-plex
summary: How to create a firewalld service definition for Plex Media Server
status: published
category: til
---

I recently moved my Plex Media Server from a Docker container to installed directly on a server. This post isn't about why I did that.

There is not firewalld service definition included, so in order to make managing that easier I created the configuration.

First, create the firewalld service at `/etc/firewalld/services/plex.xml`

```xml
<?xml version="1.0" encoding="utf-8"?>
<service>
  <short>Plex</short>
  <description> Plex Media Server (PMS) is the back-end media server component of Plex. </description>
  <port port="32400" protocol="tcp"/>
  <port port="32400" protocol="udp"/>
  <port port="32469" protocol="tcp"/>
  <port port="1900" protocol="udp"/>
  <port port="3005" protocol="tcp"/>
  <port port="8324" protocol="tcp"/>
  <port port="32410" protocol="udp"/>
  <port port="32412" protocol="udp"/>
  <port port="32413" protocol="udp"/>
  <port port="32414" protocol="udp"/>
  <port port="5353" protocol="udp"/>   
</service>
```

Then enable this service in firewalld

```sh
firewall-cmd --add-service plex                                                                                                                               
success
```
