---
title: Check SSH Key Fingerprints
date: 2021-08-19 10:00:00
modified: 2024-12-13 12:00:00
---

Show the fingerprint of an SSH key

``` {.sourceCode .shell}
$ ssh-keygen -l -f ~/.ssh/id_rsa
```

The default hashing is sha256, however it might be more useful to see the fingerprint in md5.

``` {.sourceCode .shell}
$ ssh-keygen -l -f ~/.ssh/id_rsa
```

Less useful, but sometimes fun, is showing the randomart assocated with the key

``` {.sourceCode .shell}
~ $ ssh-keygen -l -v -f .ssh/id_rsa.pub 
3072 SHA256:XglqOOv5Vu3/LR6/YADZv7Xqq2Bo9lUKq20NOmiZ7rQ jdm@raspberrypi (RSA)
+---[RSA 3072]----+
|                 |
|          o      |
|        .o .     |
|     . . ....    |
|    o o Soo. o . |
|     + .oo+ + o .|
|    ..+=o=oo +.. |
|   ..*=o=.+.. ++ |
|    *E..oo o+*+o+|
+----[SHA256]-----+
```
