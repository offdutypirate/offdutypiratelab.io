---
title: Delete lines in Vim beginning with a specific character
date: 2025-01-13
tags: vim
keywords: vim
summary: Delete lines in Vim beginning with a specific character like the hash or blank lines
status: published
related_posts: printing-line-numbers-with-cat-and-grep
category: til
---

Remove all comments from a Bash script

```text
:g/^\#/d
```

Remove all blank lines

```text
:g/^$/d
```
