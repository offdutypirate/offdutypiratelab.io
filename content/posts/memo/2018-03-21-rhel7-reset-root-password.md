---
Title: Reset root password on RHEL7
Date:  2018-03-21
---

How to reset an unknown root password using rd.break on RHEL 7.

Start by booting until the GRUB2 menu appears then edit the boot loader
menu with the e key. Find the line beginning with linux and make then
follow these steps.

1.  add rd.break
2.  run mount -o remount,rw /sysroot
3.  run chroot /sysroot
4.  run passwd
5.  run touch /.autorelabel
6.  exit chroot
7.  run mount -o remount,ro /sysroot
8.  exit resuce mode

Login with the password set in step 4 after rebooting.
