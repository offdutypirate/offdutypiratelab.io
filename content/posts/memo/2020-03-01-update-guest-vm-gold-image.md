---
title: Update libvirt/KVM guest virtual machine templates
date: 2020-03-01 20:28:00
---

virt-customize can be used to customize a virtual machine image by
installing packages, editing configuration files, and so on. This is
done by modifying the disk image in place.

Update the template
-------------------

Run yum update to update the packages already installed in the template
to their latest versions.

``` {.sourceCode .shell}
$ virt-customize --connect qemu:///system \
  -d centos7-gold \
  --selinux-relabel \
  --update
```

Adding `--selinux-relabel` is important if the guest image is using
SELinux.
