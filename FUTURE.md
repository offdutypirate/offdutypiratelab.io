# Future Topics

I often think of a _good idea_ for something to post about, but don't really have the drive to do the work at the time.  This is an attempt to record some of those ideas in the hope that maybe at a future date I'll actually draft up something. 

## Ideas

- Creating a caching reverse proxy server with Apache httpd.
- Using systemd timers instead of cron
- Document Proxmox setup and configuration 

