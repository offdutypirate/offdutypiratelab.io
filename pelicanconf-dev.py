AUTHOR = 'Jon M'
SITENAME = "Captain's Log"
SITEURL = "https://www.captainslog.me"
GITSITEURL = "https://git.sr.ht/~offdutypirate/captainslog.me/tree/master/item/content/memos"

THEME = 'theme'
STYLESHEET_URL = '/theme/style.css'

# Content and Ouput Settings
PATH = "content"

PAGE_PATHS = ['pages']
PAGE_URL = 'pages/{slug}.html'
PAGE_SAVE_AS = 'pages/{slug}.html'

ARTICLE_PATHS = ['posts']
ARTICLE_URL = 'memos/{date:%Y-%m-%d}-{slug}/' 
ARTICLE_SAVE_AS = 'memos/{date:%Y-%m-%d}-{slug}/index.html'

DEFAULT_CATEGORY = 'memo'

CATEGORIES_SAVE_AS = 'categories.html'
#CATEGORY_SAVE_AS = 'category/{slug}.html'
CATEGORY_SAVE_AS = '' 

TAGS_SAVE_AS = AUTHOR_SAVE_AS = DRAFT_SAVE_AS = ''
ARCHIVES_SAVE_AS = TAG_SAVE_AS = AUTHORS_SAVE_AS =  ''

STATIC_PATHS = ['static']

EXTRA_PATH_METADATA = {
    'static/favicon.ico' : {'path': 'favicon.ico'},
    'static/robots.txt' : {'path': 'robots.txt'},
    'static/keybase.txt' : {'path': '.well-known/keybase.txt'},
    }


TIMEZONE = 'America/Chicago'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Menu
MENUITEMS = (
  ("GPG", "/static/gpg-public-key.asc"),
)

# Blogroll
LINKS = (
#    ("Pelican", "https://getpelican.com/"),
#    ("Python.org", "https://www.python.org/"),
#    ("Jinja2", "https://palletsprojects.com/p/jinja/"),
#    ("You can modify those links in your config file", "#"),
)

# Social widget
SOCIAL = (
  ("Mastodon", "https://mastodon.sdf.org/@offdutypirate"),
    #("Another social link", "#"),
)

#DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True

DISPLAY_CATEGORIES_ON_MENU = False
DISPLAY_PAGES_ON_MENU = True

PLUGINS = [
  'pelican.plugins.touch',
  'pelican.plugins.jinja_filters',
  'pelican.plugins.related_posts',
  'pelican.plugins.sitemap',
  # TODO: Need to fix template before enabling the neighbor plugin.
  'pelican.plugins.neighbors',
  # TODO: Need to fix show-source SITE URL
  #'show_source',
 ] 

MARKDOWN = {
    'extension_configs': {
        'markdown.extensions.extra': {},
        'markdown.extensions.meta': {},
        'markdown.extensions.toc': {},
        'markdown.extensions.admonition': {},
    },
    'output_format': 'html5',
}

RELATED_POSTS_MAX = 10

# Sitemap
SITEMAP = {
    "format": "xml",
    "priorities": {
        "articles": 0.5,
        "indexes": 0.5,
        "pages": 0.5
    },
    "changefreqs": {
        "articles": "monthly",
        "indexes": "daily",
        "pages": "monthly"
    }
}

SHOW_SOURCE_ALL_POSTS = True
SHOW_SOURCE_IN_SECTION = True
SHOW_SOURCE_PRESERVE_EXTENSION = True